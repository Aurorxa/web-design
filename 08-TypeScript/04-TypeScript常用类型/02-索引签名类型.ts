interface MyArray<T> {
    [n: number]: T
}

const arr: MyArray<number> = [1, 2, 3]
