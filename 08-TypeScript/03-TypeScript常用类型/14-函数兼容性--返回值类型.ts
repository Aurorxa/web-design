type F1 = () => { name: string }
type F2 = () => { name: string; age: number }
let f1: F1
let f2: F2

f1 = f2
