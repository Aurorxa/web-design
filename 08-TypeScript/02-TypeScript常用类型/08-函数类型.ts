// 如果函数没有返回值，那么，函数的返回值类型就是 void 
function add(num1: number, num2: number): void {
    console.log(num1 + num2);
}

add(1, 2);