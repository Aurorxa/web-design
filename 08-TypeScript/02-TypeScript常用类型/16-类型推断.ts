// 声明变量并初始化。
let age = 18;
console.log(age);

// 决定函数返回值的时候
function add(num1: number, num2: number) {
    return num1 + num2;
}

let sum = add(1, 2);
console.log(sum);
