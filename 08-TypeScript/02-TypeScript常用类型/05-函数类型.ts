// 单独指定参数、返回值的类型
function add(num1: number, num2: number): number {
    return num1 + num2;
}

let sum = add(1, 2);
console.log(sum);
