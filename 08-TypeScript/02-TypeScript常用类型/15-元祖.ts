// 元祖类型可以确切的知道包含了多少个元素，以及特定索引对应的类型
let position: [number, number] = [39.5727, 116.2317];
console.log(position);