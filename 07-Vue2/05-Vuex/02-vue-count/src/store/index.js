// 该文件用于 Vuex 中最为核心的 store
import Vue from 'vue';
// 引入 Vuex
import Vuex from 'vuex';
// 使用插件
Vue.use(Vuex);
// 创建 store
export default new Vuex.Store({
    state: { // 准备 state ，用于存储数据

    },
    actions: { // 准备 actions ，用于响应组件中的动作

    },
    mutations: { // 准备 mutations ，用于操作数据（state）

    }
});
