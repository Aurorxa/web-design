/* 使用分别暴露 */
export const uname = '许大仙';
export const age = 18;

export function showUname() {
    console.log(uname);
}

export function showAge() {
    console.log(age);
}