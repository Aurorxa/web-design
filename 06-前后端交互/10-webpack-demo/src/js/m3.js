// 使用默认暴露
export default {
    name: '江苏科技大学',
    address: '中国江苏省镇江市京口区梦溪路2号',
    subjects: ['前端', '大数据', 'Python']
};